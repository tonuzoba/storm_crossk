import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.path as pth
import seaborn as sns
from astropy.stats import RipleysKEstimator
import sklearn.neighbors as sn
from shapely.geometry import Polygon
import re
from itertools import compress
pd.options.display.max_colwidth=300
'''
### RipleysKEstimator arguments:
* data - 2D array - represents the set of observed points in the area of the study
* radii - set of distances for which the estimator evaluates
* mode - method of edge correction (none, translation, ohser, var-width, ripley)
'''
# RoiAttr files
roiattr_files= !ls /mnt/csoport43-2/Gyula/STORM_alignment_investigation_data/data/Roiattr_GABA_files/*COORD.txt
files=pd.DataFrame(data=roiattr_files, columns=['RoiAttr'])
files['index']=files['RoiAttr'].map(lambda x: x[82:90]+ str('_') + x[121:123]  + '_'
                                    + re.search(r'\w*_(\d+)_RoiAttr_COORD.txt', x).group(1))
# files['roi_no']=files['RoiAttr'].map(lambda x: re.search(r'\w*_(\d+)_RoiAttr_COORD.txt', x).group(1))
files.set_index('index', inplace=True)
files.head(3)

#RoiCoord files
roicoord_files=! ls /mnt/csoport43-2/Gyula/STORM_alignment_investigation_data/data/RoiCoord_GABA_files/*.txt
files_coord=pd.DataFrame(data=roicoord_files, columns=['RoiCoord'])
files_coord['index']=files_coord['RoiCoord'].map(lambda x: x[83:91]+ str('_') + x[122:124] + '_' +
                                                re.search(r'\w*_(\d+)_RoiCoord.txt', x).group(1))
files_coord.set_index('index', inplace=True)
files_coord.head(3)

#defining groups
def group_id(Cell):
    if Cell=='BB185':
        return 'High'
    if Cell=='BB193':
        return 'High'
    if Cell=='BB192':
        return 'Ctrl'
    if Cell=='BB203':
        return 'Ctrl'
    if Cell == 'BB222':
        return 'Low'
    if Cell == 'BB226':
        return 'Low'
    if Cell == 'BB238':
        return 'Low'
    if Cell == 'BB239':
        return 'Low'

# Unified, indexed table with RoiCoord and RoiAttr pathes with Cell and Group names included
files['RoiCoord']=files.index.map(files_coord['RoiCoord'])
files['Cell']=files.index.map(lambda x: x[:5])
files['Group']=files['Cell'].apply(lambda cell: group_id(cell))
files=files.dropna()
files.head()

# number of file pairs per group
sns.set()
files.dropna().groupby(by='Group').count().plot(y='Cell', kind='bar', figsize=(8,6))
plt.legend('')

# defining RoiAttr Polygon
def polygon_area(path):
    '''
Reads a coordinate file and returns a  polygon.
Needs  shapely
'''
    df=pd.read_csv(path, sep='\t', header='infer')
    roizip=zip(df.x, df.y)
    roilist=list(roizip)
    polyarea=Polygon(roilist).area
    return polyarea

#defining polygon area
pr=polygon_area('/mnt/csoport43-2/Gyula/STORM_alignment_investigation_data/data/Roiattr_GABA_files/BB185_02_934_CB1_647_1116_Bsn_568_storm03_list-2018-06-25-19-18-35_S03_activeContourROI_19_RoiAttr_COORD.txt')
pr

# Example file
coord01=pd.read_csv(files.iloc[0,1],
                   header='infer', sep='\t')
n=len(coord01.loc[coord01['Channel Name']=='405/647']) # number of LPs in the STORM stack
n

xx=coord01.loc[coord01['Channel Name']=='405/647']['X'] # x coordinates of LP
yy=coord01.loc[coord01['Channel Name']=='405/647']['Y'] # y coordinate of LP
real=list(zip(xx, yy))  # zipped x+y coords
real[:10]

#reading in RoiAttr file
attr01=pd.read_csv(files.iloc[0,0],
                   header='infer', sep='\t')
vtx=list(zip(attr01['x'], attr01['y'])) # coordinates for RoiAttr polygon
plt.plot(attr01['x'], attr01['y'])

# bounding box coordinates of the polygons
xlow=attr01.x.min()
xhigh=attr01.x.max()
ylow=attr01.y.min()
yhigh=attr01.y.max()

#generating n random points inside the bounding box
zx=np.random.uniform(low=xlow, high=xhigh, size=n)
zy=np.random.uniform(low=ylow, high=yhigh, size=n)
zxy=list(zip(zx,zy))
#print(len(zxy))

contain=pth.Path(vtx).contains_points(zxy)
filtered=list(compress(zxy, contain))
ins=len(filtered)
ins                   # number of points inside the polygon

# generating n points inside the polygon
while ins<n:
    zx1=np.random.uniform(low=xlow, high=xhigh, size=(n-ins))
    zy1=np.random.uniform(low=ylow, high=yhigh, size=(n-ins))
    zxy1=list(zip(zx1,zy1))
    zxy=zxy+zxy1
#     print(zxy)/
    contain=pth.Path(vtx).contains_points(zxy)
    filtered=list(compress(zxy, contain))
    ins=len(filtered)

print(n, ins, sep="\n") # prints real and generated LP number inside the polygon

fx,fy=zip(*filtered)  # unzips generated uniform x and y coordinates
len(fy)

# plots the borders of the investigated bouton, the generated uniform and the
# real distribution
plt.figure(figsize=(8,6), dpi=300)
plt.plot(attr01['x'], attr01['y'], 'b--')
plt.plot(fx,fy, 'go', markersize=5, markeredgecolor="w", markeredgewidth=0.5)
plt.plot(xx,yy, 'ro', markersize=5, markeredgecolor="w", markeredgewidth=0.5)

# generates Ripley's K function plots from the uniform, the poisson and
# real distributions
sns.set(style="whitegrid", context="poster")
plt.figure(figsize=(10,10))
r=np.linspace(0,300,100)
Kest=RipleysKEstimator(area=pr, x_min=xlow, x_max=xhigh, y_min=ylow, y_max=yhigh)
plt.plot(r, Kest.poisson(r), color='blue', ls='--', label=r'$K_{pois}$')
plt.plot(r, Kest(data=filtered, radii=r, mode='ripley'), color='red', ls='--', label=r'$K_{uniform}$')
plt.plot(r, Kest(data=real, radii=r, mode='ripley'), color='cyan', ls='--', label=r'$K_{GABA\ bouton}$')
plt.legend()
