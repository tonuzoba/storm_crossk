import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.path as pth
import seaborn as sns
from astropy.stats import RipleysKEstimator
from shapely.geometry import Polygon
import re
from itertools import compress
pd.options.display.max_colwidth=300

# RoiAttr files
roiattr_files= !ls /mnt/csoport43-2/Gyula/STORM_alignment_investigation_data/data/Roiattr_GABA_files/*COORD.txt
files=pd.DataFrame(data=roiattr_files, columns=['RoiAttr'])
files['index']=files['RoiAttr'].map(lambda x: x[82:90]+ str('_') + x[121:123]  + '_'
                                    + re.search(r'\w*_(\d+)_RoiAttr_COORD.txt', x).group(1))
files.set_index('index', inplace=True)

#RoiCoord files
roicoord_files=! ls /mnt/csoport43-2/Gyula/STORM_alignment_investigation_data/data/RoiCoord_GABA_files/*.txt
files_coord=pd.DataFrame(data=roicoord_files, columns=['RoiCoord'])
files_coord['index']=files_coord['RoiCoord'].map(lambda x: x[83:91]+ str('_') + x[122:124] + '_' +
                                                re.search(r'\w*_(\d+)_RoiCoord.txt', x).group(1))
files_coord.set_index('index', inplace=True)

#defining groups
def group_id(Cell):
    if Cell=='BB185':
        return 'High'
    if Cell=='BB193':
        return 'High'
    if Cell=='BB192':
        return 'Ctrl'
    if Cell=='BB203':
        return 'Ctrl'
    if Cell == 'BB222':
        return 'Low'
    if Cell == 'BB226':
        return 'Low'
    if Cell == 'BB238':
        return 'Low'
    if Cell == 'BB239':
        return 'Low'

# Unified, indexed table with RoiCoord and RoiAttr pathes with Cell and Group names included
files['RoiCoord']=files.index.map(files_coord['RoiCoord'])
files['Cell']=files.index.map(lambda x: x[:5])
files['Group']=files['Cell'].apply(lambda cell: group_id(cell))
files=files.dropna()

# defining RoiAttr Polygon
def polygon_area(path):
    '''
Reads a coordinate file and returns a  polygon.
Needs  shapely
'''
    df=pd.read_csv(path, sep='\t', header='infer')
    roizip=zip(df.x, df.y)
    roilist=list(roizip)
    polyarea=Polygon(roilist).area
    return polyarea

def draw_bouton_ripley(path_roicoord, path_roiattr):
    '''
path_roicoord - path to the given bouton's roicoord files. Text file generated
from vividstorm containing the interesting informations from the given storm
localizations.
path_roiattr - path to the given bouton's roiattr files. Has to contain the x
and y coordinates of the ROI only.

    '''
    coord=pd.read_csv(path_roicoord, header='infer', sep='\t')
    n=len(coord.loc[coord['Channel Name']=='405/647']) # number of LPs in the STORM stack
    xx=coord.loc[coord['Channel Name']=='405/647']['Xwc'] # x coordinates of LP
    yy=coord.loc[coord['Channel Name']=='405/647']['Ywc'] # y coordinate of LP
    real=list(zip(xx, yy))  # zipped x+y coords
    # real[:10]
    #reading in RoiAttr file
    attr01=pd.read_csv(path_roiattr, header='infer', sep='\t')
    vtx=list(zip(attr01['x'], attr01['y'])) # coordinates for RoiAttr polygon
    # plt.plot(attr01['x'], attr01['y']) # plots bouton polygon
    # bounding box coordinates of the polygons
    xlow=attr01.x.min()
    xhigh=attr01.x.max()
    ylow=attr01.y.min()
    yhigh=attr01.y.max()
    #generating n random points inside the bounding box
    zx=np.random.uniform(low=xlow, high=xhigh, size=n)
    zy=np.random.uniform(low=ylow, high=yhigh, size=n)
    zxy=list(zip(zx,zy))
    #print(len(zxy))
    contain=pth.Path(vtx).contains_points(zxy)
    filtered=list(compress(zxy, contain))
    ins=len(filtered)
    ins                   # number of points inside the polygon
    # generating n points inside the polygon
    while ins<n:
        zx1=np.random.uniform(low=xlow, high=xhigh, size=(n-ins))
        zy1=np.random.uniform(low=ylow, high=yhigh, size=(n-ins))
        zxy1=list(zip(zx1,zy1))
        zxy=zxy+zxy1
    #     print(zxy)/
        contain=pth.Path(vtx).contains_points(zxy)
        filtered=list(compress(zxy, contain))
        ins=len(filtered)
    print(f'Number of real points: {n} \nNumber of generated points: {ins}')
    # prints real and generated LP number inside the polygon
    fx,fy=zip(*filtered)  # unzips generated uniform x and y coordinates
    # len(fy)
    # plots the borders of the investigated bouton, the generated uniform and the
    # real distribution

    sns.set(style="white", context="paper")
    f=plt.figure(figsize=(8,6), dpi=300)
    plt.plot(attr01['x'], attr01['y'], 'b--', label='_nolegend_')
    plt.plot(fx,fy, 'go', markersize=5, markeredgecolor="w", markeredgewidth=0.5, label='Uniform')
    plt.plot(xx,yy, 'ro', markersize=5, markeredgecolor="w", markeredgewidth=0.5, label='Real')
    plt.legend(loc=2)
    fpath, fname = os.path.split(path_roicoord)
    code =  re.search(r'(BB\d{3}_\d{2}_).*storm(\d{2}_).*_(\d+)_RoiCoord.txt', fname)
    idf = str(code.group(1) + code.group(2) + code.group(3) + '_bouton.png')
    base = '/mnt/csoport43-2/Gyula/STORM_alignment_investigation_data/data/plots/'
    filename_bouton = os.path.join(base, idf)
    f.savefig(filename_bouton, bbox_inches='tight')

    # plot Ripley's function plots
    sns.set(style="whitegrid",  context="paper")
    g=plt.figure(figsize=(8,8), dpi=300)
    r=np.linspace(0,300,100)
    pr=polygon_area(path_roiattr)
    Kest=RipleysKEstimator(area=pr, x_min=xlow, x_max=xhigh, y_min=ylow, y_max=yhigh)
    plt.plot(r, Kest.poisson(r), color='blue', ls='--', label=r'$K_{pois}$')
    plt.plot(r, Kest(data=filtered, radii=r, mode='ripley'), color='red', ls='--', label=r'$K_{uniform}$')
    plt.plot(r, Kest(data=real, radii=r, mode='ripley'), color='cyan', ls='--', label=r'$K_{GABA\ bouton}$')
    plt.legend()
    idr = str(code.group(1) + code.group(2) + code.group(3) + '_ripley.png')
    filename_ripley = os.path.join(base, idr)
    g.savefig(filename_ripley, bbox_inches='tight')
